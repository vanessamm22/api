//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

var bodyparser = require('body-parser');
app.use(bodyparser.json());

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

var movimientosJSON = require('./Movimientosv2.json');

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/Clientes', function(req, res) {
  res.send('Aqui estan los clientes devueltos nuevos');
});

app.get('/Clientes/:idCliente', function(req, res) {
  res.send('Aqui tiene el cliente numero: ' + req.params.idCliente);
});

app.get('/Movimientos/v1', function(req, res) {
  res.sendfile('Movimientosv1.json');
});

/*app.get('/Movimientos/v2', function(req, res) {
  res.sendfile('Movimientosv2.json');
});*/

app.get('/Movimientos/v2', function(req, res) {
  res.send(movimientosJSON);
});


app.get('/Movimientos/v2/:index/:otroParametro', function(req, res) {
  console.log(req.params.index);
  console.log(req.params.otroParametro);
  res.send(movimientosJSON[req.params.index-1]);
  res.send(movimientosJSON[req.params.otroPrametro]);
  //res.sendfile('Movimientosv2.json');
});

app.get('/Movimientosq/v2', function(req, res) {
  console.log(req.query);
  res.send('Recibido');
  //res.sendfile('Movimientosv2.json');
});

app.post('/',function(req,res){
 res.send('Hemos recibido su petición post');
})

app.post('/Movimientos/v2',function(req,res){
  var nuevo = req.body
  nuevo.id = movimientosJSON.length + 1;
  console.log(nuevo);
  movimientosJSON.push(nuevo)
  res.send('Movimiento dado de alta');
})
